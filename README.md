# NFT-PREVIEW-CARD using BEM

This is a basic project that shows a correct implementation of the BEM methodology (Figma exercise design is attached). 

## Technologies used

 - HTML
 - SASS
 - CSS
 - BEM 

Como recomendacion, para no tener que configurar SASS desde cero, pueden utilizarse las siguientes extensiones de Visual Studio Code:

 - [Live Server](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer): Used to automatically detect enhanced settings in code and as a prerequisite for conversion from SASS to CSS without configuration.
 - [Live SASS](https://marketplace.visualstudio.com/items?itemName=ritwickdey.live-sass): Used to automatically detect enhanced settings in code and as a prerequisite for conversion from SASS to CSS without configuration.
